RUSTC=rustc
RUSTFLAGS=-A unused_variable -A dead_code
LDFLAGS=-L lib

.PHONY : all clean doc

all: clean compile

debug: RUSTFLAGS += -g -Z time-passes
debug: compile

fast: RUSTFLAGS += -O
fast: compile

compile:
	mkdir -p bin
	$(RUSTC) $(RUSTFLAGS) -o bin/minesweeper $(LDFLAGS) src/main.rs

deps:

doc:
	rustdoc $(LDFLAGS) src/main.rs

clean:
	rm -f bin/**

run:
	bin/minesweeper
