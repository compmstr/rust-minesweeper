use std::fmt::{Show, Formatter, Result};
use std::rand::{StdRng, Rng};
use ansi::{White, Black, fg_color, bg_color, set_attr_str, attr, Reverse, Normal};
use std::string::String;

pub enum GameStatus{
    Won,
    Lost,
    Playing,
}
#[deriving(Clone)]
pub struct Cell{
    mine: bool,
    marked: bool,
    revealed: bool,
    near_mines: uint,
}
impl Cell{
    pub fn new() -> Cell {
        Cell{ mine: false, marked: false, revealed: false, near_mines: 0 }
    }
    pub fn display_char(&self) -> char {
        if self.revealed {
            if self.mine {
                '*'
            } else {
                ('0' as uint + self.near_mines) as u8 as char
            }
        } else if self.marked {
            '?'
        } else{
            '-'
        }
    }
}

pub struct Board{
    cells: Vec<Cell>,
    mine_count: uint,
    pub width: uint,
    pub height: uint,
}

impl Board{
    fn cell_idx(&self, x: uint, y: uint) -> uint {
        y * self.width + x
    }
    fn cell_coords(&self, idx: uint) -> (uint, uint) {
        (idx % self.width, idx / self.width)
    }
    fn add_mines(&mut self){
        let mut rand = StdRng::new().unwrap();
        let mut set = 0;
        while set < self.mine_count {
            let x = rand.gen_range(0u, self.width);
            let y = rand.gen_range(0u, self.height);
            if !self.cells.get(self.cell_idx(x, y)).mine {
                set += 1;
                self.cells.get_mut(self.cell_idx(x, y)).mine = true;
            }
        }
    }
    fn surround_idx(&mut self, idx: uint) -> Vec<uint> {
        let (x, y) = self.cell_coords(idx);
        self.surround_coords(x, y)
    }
    fn surround_coords(&mut self, x: uint, y: uint) -> Vec<uint>{
        use std::iter::range_inclusive;
        let mut ret: Vec<uint> = Vec::new();
        for dx in range_inclusive(-1, 1) {
            for dy in range_inclusive(-1, 1) {
                let cur_x = x as int + dx;
                let cur_y = y as int + dy;
                if cur_x >= 0 && cur_x < self.width as int &&
                    cur_y >= 0 && cur_y < self.height as int {
                        ret.push(self.cell_idx(cur_x as uint, cur_y as uint));
                    }
            }
        }
        ret
    }
    fn generate_numbers(&mut self){
        for idx in range(0u, self.width * self.height){
            let mut count = 0u;
            for other_idx in self.surround_idx(idx).iter() {
                if self.cells.get(*other_idx).mine {
                    count += 1;
                }
            }
            self.cells.get_mut(idx).near_mines = count;
        }
    }
    pub fn new(w: uint, h: uint, mine_count: uint) -> Board{
        let cells: Vec<Cell> = range(0u, w * h).map(|_| Cell::new()).collect();

        let mut b = Board{
            cells: cells,
            mine_count: mine_count,
            width: w,
            height: h,
        };
        b.add_mines();
        b.generate_numbers();
        b
    }
    
    pub fn mark(&mut self, x: uint, y: uint) {
        let idx = self.cell_idx(x, y);
        if !self.cells.get(idx).revealed {
            self.cells.get_mut(idx).marked = true;
        }else{
            println!("Unable to mark revealed cell!");
        }
    }
    fn reveal_idx(&mut self, idx: uint){
        let cell = self.cells.get(idx);
        if cell.revealed { return; }
        if !cell.marked {
            self.cells.get_mut(idx).revealed = true;
        }else{
            println!("Unable to reveal marked cell");
        }
        if cell.near_mines == 0 {
            //Reveal all the surrounding cells
            for cur in self.surround_idx(idx).iter() {
                self.reveal_idx(*cur);
            }
        }
    }
    pub fn reveal(&mut self, x: uint, y: uint) {
        let idx = self.cell_idx(x, y);
        self.reveal_idx(idx);
    }
    
    //TODO- win if all remaining hidden mines are marked, and no extra marks
    pub fn status(&self) -> GameStatus {
        if self.cells.iter().any(|cell| cell.mine && cell.revealed) {
            Lost
        } else if self.cells.iter().all(|cell| cell.mine || cell.revealed || cell.marked) {
            Won
        } else {
            Playing
        }
    }
}

impl Show for Board{
    fn fmt(&self, f: &mut Formatter) -> Result {
        let mut out = String::new();
        
        //Col numbers
        out.push_str("  ");
        for x in range(0, self.width){
            out.push_str(format!("{:>2u}", x).as_slice());
        }
        //Body
        for y in range(0, self.height) {
            out.push_char('\n');
            //Row number
            out.push_str(format!("{:>2u}", y).as_slice());
            out.push_str(set_attr_str([attr(Reverse)]).as_slice());
            for x in range(0, self.width) {
                out.push_char(' ');
                out.push_char(self.cells.get(self.cell_idx(x, y)).display_char());
            }
            out.push_str(set_attr_str([attr(Normal)]).as_slice());
        }

        out.push_char('\n');
        for i in range(0, self.width * 2 + 2) {
            out.push_char('=');
        }

        f.pad(out.as_slice())
    }
}
