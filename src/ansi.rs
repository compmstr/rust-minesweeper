#[allow(attribute_usage)]
//#![feature(macro_rules)]

use std::io::stdio::print;
use std::string::String;
use std::owned::Box;

pub fn ansi_str(command: &str) -> String{
    String::from_str("\x1B[").append(command)
}

pub fn move_cursor_str(row: int, col: int) -> String{
    let mut buf = String::new();
    buf.push_str(row.to_str().as_slice());
    buf.push_char(';');
    buf.push_str(col.to_str().as_slice());
    buf.push_char('H');
    ansi_str(buf.into_owned().as_slice())
}
pub fn move_cursor(row: int, col: int){
    print(move_cursor_str(row, col).as_slice());
}

pub fn clear_str() -> String {ansi_str("2J")}
pub fn clear(){print(clear_str().as_slice());}

pub fn save_cursor_str() -> String {ansi_str("s")}
pub fn save_cursor(){print(save_cursor_str().as_slice());}

pub fn restore_cursor_str() -> String {ansi_str("u")}
pub fn restore_cursor(){print(restore_cursor_str().as_slice());}

pub fn hide_cursor_str() -> String {ansi_str("?25l")}
pub fn hide_cursor(){print(hide_cursor_str().as_slice());}

pub fn show_cursor_str() -> String {ansi_str("?25h")}
pub fn show_cursor(){print(show_cursor_str().as_slice());}

macro_rules! saving_location(
    ($body:expr) => (
        save_cursor();
        $body
        restore_cursor();
    );
)
macro_rules! at_xy(
    ($x:expr, $y:expr, $body:expr) => ({
        save_cursor();
        move_cursor($y, $x);
        $body
        restore_cursor();
    });
)

pub enum Color {
    Black,
    Red,
    Green,
    Yellow,
    Blue,
    Magenta,
    Cyan,
    White
}
pub enum Attr{
    Normal = 0,
    Bold,
    Blink = 5,
    Reverse = 7,
    Concealed
}
pub fn attr(att: Attr) -> int {att as int}
pub fn fg_color(col: Color) -> int {col as int + 30}
pub fn bg_color(col: Color) -> int {col as int + 40}

pub fn set_attr_str(elts: &[int]) -> String {
    let strs: Vec<String> = elts.iter().map(|x| x.to_str()).collect();
    let mut str = strs.connect(";");
    ansi_str(str.append("m").as_slice())
}
pub fn set_attrs(elts: &[int]){
    print(set_attr_str(elts).as_slice());
}

#[allow(dead_code, unused_must_use)]
fn main(){
    use std::io;
    clear();
    move_cursor(10, 10);
    print("foobar");
    set_attrs([attr(Bold), fg_color(Green), bg_color(Blue)]);
    move_cursor(11, 10);
    print("baz");
    at_xy!(5, 5, {println!("haha");});
    println!(" more?");
    io::stdin().read_byte();
    for c in set_attr_str([attr(Bold), fg_color(Green), bg_color(Blue)]).as_slice().chars() {
        println!("{:d} - {:c}", c as i8, c);
    }
}
