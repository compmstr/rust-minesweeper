#![feature(macro_rules)]
extern crate rand;
use std::io::BufferedReader;
use ansi::{clear, set_attrs, attr, fg_color};
use std::from_str::from_str;
use board::{Won, Lost, Playing};

#[allow(dead_code)]
mod ansi;
mod board;

fn play(board: &mut board::Board){
    fn convert_coords(board: &board::Board, x: &str, y: &str) -> Option<(uint, uint)>{
        let x: uint = match from_str(x){
            Some(x) => x,
            None => return None,
        };
        let y: uint = match from_str(y){
            Some(y) => y,
            None => return None,
        };
        if x >= board.width || y >= board.height {
            None
        }else{
            Some((x, y))
        }
    }
    let mut reader = BufferedReader::new(std::io::stdin());
    'turn : loop {
        println!("{}", *board);
        print!("(m)ark|(r)eveal x y : ");
        let cmd = reader.read_line().ok().unwrap();
        
        let tokens: Vec<&str> = cmd.as_slice().split(|c: char| c.is_whitespace())
            .filter(|s| s.len() != 0).collect();
        println!("{}", tokens.as_slice());
        match tokens.as_slice() {
            ["q"] => break 'turn,
            ["m", x, y] => {
                match convert_coords(board, x, y){
                    Some((x, y)) => {
                        board.mark(x, y);
                    },
                    None => {
                        println!("Invalid coordinates");
                        continue 'turn;
                    },
                }
            },
            ["r", x, y] => {
                match convert_coords(board, x, y){
                    Some((x, y)) => {
                        board.reveal(x, y);
                    },
                    None => {
                        println!("Invalid coordinates");
                        continue 'turn;
                    },
                }
            },
            _ => println!("Invalid command"),
        }
        //println!("{:?}", board.status());
        match board.status() {
            Playing => clear(),
            Won => {
                println!("You won!");
                break 'turn;
            },
            Lost => {
                println!("You lost!... loser.");
                break 'turn;
            },
        };
    }
}

fn main(){
    let mut reader = BufferedReader::new(std::io::stdin());
    'choice : loop{
        println!("Minesweeper");
        println!("-----------");
        
        println!("Please Choose a level:");
        println!("1- Standard (9x9, 10 mines)");
        println!("2- Medium (20x20, 30 mines)");
        println!("q- Quit");
        println!("--------------------------");
        print!("> ");
        let choice_raw = reader.read_line().ok().unwrap();
        let choice = choice_raw.as_slice().trim_right();
        match choice {
            "1" => {
                clear();
                play(&mut board::Board::new(9, 9, 10));
            },
            "2" => {
                clear();
                play(&mut board::Board::new(20, 20, 30));
            },
            "q" => {
                return
            },
            _ => {
                clear();
                set_attrs([attr(ansi::Bold), fg_color(ansi::Red)]);
                println!("Invalid choice")
                set_attrs([attr(ansi::Normal), fg_color(ansi::White)]);
            },
        }
    }
}
